﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using App3.Models;

namespace App3
{
	public partial class MainPage : ContentPage
	{
		public MainPage()
		{
			InitializeComponent();
		}

        private  void OnClick_btn_Enviar(object sender, EventArgs e)
        {
            Info i = new Info();
            i.Tarefa = Tarefa.Text;
            i.Nome = Nome.Text;
            i.Email = Email.Text;
            i.DescTar = DescTar.Text;
            i.DI = DI.Date.ToShortDateString();
            i.TI = TI.Time.ToString();
            i.DF = DF.Date.ToShortDateString();
            i.TF = TF.Time.ToString();
            string publicar = "Não";
           if(Publicar.IsToggled)
            {
                publicar = "Sim";
            }
            i.Publicar = "Publicar a Tarefa? " + publicar;
            Application.Current.MainPage = new NavigationPage(new Page1(i));
        }

    }
}
