﻿using App3.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace App3
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class Page1 : ContentPage
	{
		public Page1 (Info i)
		{

			InitializeComponent ();
            Tarefa.Text = i.Tarefa;
            Nome.Text = i.Nome;
            Email.Text = i.Email;
            DescTar.Text = i.DescTar;
            DI.Text = i.DI;
            TI.Text = i.TI;
            DF.Text = i.DF;
            TF.Text = i.TF;
            Publicar.Text = i.Publicar;

        }
	}
}