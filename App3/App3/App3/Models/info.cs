﻿using System;
using System.Collections.Generic;
using System.Text;

namespace App3.Models
{
    public class Info
    {
        public string Tarefa { get; set; }
        public string Nome { get; set; }
        public string Email { get; set; }
        public string DescTar { get; set; }
        public string DI { get; set; }
        public string TI { get; set; }
        public string DF { get; set; }
        public string TF { get; set; }
        public string Publicar { get; set; }
    }
}
